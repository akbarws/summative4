package com.akbar.summative4.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.akbar.summative4.entity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

	@Query(value = "SELECT * FROM transaction WHERE DATE(created_at) = :date", nativeQuery = true)
	List<Transaction> findByDateParam(@Param(value = "date") String dateFromParam);
}
