package com.akbar.summative4.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.akbar.summative4.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query(value = "SELECT * "
			+ "FROM product "
			+ "WHERE product_name=:product_name OR stock=:stock OR price=:price OR category=:category", 
			nativeQuery = true)
	List<Product> search(
			@Param(value = "product_name") String product_name,
			@Param(value = "stock") String stock,
			@Param(value = "price") String price, 
			@Param(value = "category") String category
			);

}