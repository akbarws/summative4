package com.akbar.summative4.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.akbar.summative4.entity.Product;
import com.akbar.summative4.entity.Transaction;
import com.akbar.summative4.repository.ProductRepository;
import com.akbar.summative4.repository.TransactionRepository;
import com.akbar.summative4.response.HttpResponse;

@RestController
public class TransactionController {

	@Autowired
	TransactionRepository repo;
	@Autowired
	ProductRepository productRepo;
	
	@PostMapping(value = "/api/payment", consumes = "application/json", produces = "application/json")
	public ResponseEntity<HttpResponse> add(@RequestBody List<Map<String, Long>> trs) {
		List<Transaction> newTrs = new ArrayList<Transaction>();
		if(trs.size() == 1) {
			Map<String, Long> tr = trs.get(0);
			Optional<Product> products = productRepo.findById(tr.get("product_id"));
			if(products.isEmpty())
				return ResponseEntity.status(HttpStatus.OK).body(new HttpResponse(HttpStatus.OK.value(), "Product not found"));
			Product product = products.get();
			int qty = tr.get("qty").intValue();
			if(qty > product.getStock())
				return ResponseEntity.status(HttpStatus.OK).body(new HttpResponse(HttpStatus.OK.value(), "Transaction failed qty > stock"));
			
			int newStock = product.getStock() - qty;
			product.setStock(newStock);
			productRepo.save(product);
			
			Transaction trans = new Transaction(product, qty);
			int total_price = qty * product.getPrice();
			trans.setTotal_price(total_price);
			repo.save(trans);
			
			return ResponseEntity.status(HttpStatus.CREATED.value()).body(new HttpResponse(HttpStatus.CREATED.value(), "Success", trans));
		} else { 
			// kalau single bisa cek validitas product existnya dan validitas stoknya
			// kalau multi returnnya transaction yg berhasil masuk saja
			trs.forEach(tr -> {
				Optional<Product> products = productRepo.findById(tr.get("product_id"));
				if(products.isEmpty())
					return;
				Product product = products.get();
				int qty = tr.get("qty").intValue();
				if(qty > product.getStock())
					return;
				
				int newStock = product.getStock() - qty;
				product.setStock(newStock);
				productRepo.save(product);
				
				Transaction trans = new Transaction(product, qty);
				int total_price = qty * product.getPrice();
				trans.setTotal_price(total_price);
				repo.save(trans);
				newTrs.add(trans);
			});
			
			return ResponseEntity.status(HttpStatus.CREATED.value()).body(new HttpResponse(HttpStatus.CREATED.value(), "Success", newTrs));
		}
	}

	@GetMapping(value = "api/payments", produces = "application/json")
	public HttpResponse get() {
		List<Transaction> trs = repo.findAll();
		if(trs.size() == 0)
			return new HttpResponse(HttpStatus.OK.value(), "Empty", trs);
		return new HttpResponse(HttpStatus.OK.value(), "Success", trs);
	}

	@PostMapping(value = "api/payment/search", consumes = "application/json", produces = "application/json")
	public HttpResponse getByDate(@RequestBody Map<String, String> data) {
		String dateFromParam = data.get("date");
		if(!isValidDate(dateFromParam))
			return new HttpResponse(HttpStatus.OK.value(), "Date not valid");
		List<Transaction> trs = repo.findByDateParam(dateFromParam);
		if(trs.size() == 0)
			return new HttpResponse(HttpStatus.OK.value(), "No trs on that date", trs);
		return new HttpResponse(HttpStatus.OK.value(), "Success", trs);
	}
	
	private boolean isValidDate(String date){
		boolean isValid = true;
		try {  
			@SuppressWarnings("unused")
			Date parsedDate = (Date) new SimpleDateFormat("yyyy-MM-dd").parse(date);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			isValid = false;
		}
		return isValid;
	}
}
