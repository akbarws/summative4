package com.akbar.summative4.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.akbar.summative4.entity.Product;
import com.akbar.summative4.repository.ProductRepository;
import com.akbar.summative4.response.HttpResponse;

@RestController
public class ProductController {

	@Autowired
	ProductRepository repo;
	
	@PostMapping(value = "/api/product", consumes = "application/json", produces = "application/json")
	public ResponseEntity<HttpResponse> add(@RequestBody List<Product> products) {
		List<Product> newProducts = new ArrayList<Product>();
		products.forEach(product -> {
			if(validateProduct(product))
				newProducts.add(product);
		});
		List<Product> addedProducts = repo.saveAll(newProducts);
		if(addedProducts.size() == 0)
			return ResponseEntity.status(HttpStatus.CREATED).body(new HttpResponse(HttpStatus.CREATED.value(), "Nothing added."));
		return ResponseEntity.status(HttpStatus.CREATED).body(new HttpResponse(HttpStatus.CREATED.value(), "Products added", addedProducts));
		
	}
	
	private boolean validateProduct(Product product) {
		return (product.getPrice() > 0 && product.getStock() > 0);
	}
	
	@GetMapping(value = "/api/products", produces = "application/json")
	public HttpResponse getAll() {
		List<Product> products = repo.findAll();
		return new HttpResponse(HttpStatus.OK.value(), "Success", products);
	}
	
	@PutMapping(value = "/api/product/{id}", consumes = "application/json", produces = "application/json")
	public HttpResponse edit(@PathVariable Long id, @RequestBody Product product) {
		Optional<Product> getProducts = repo.findById(id);
		if(getProducts.isEmpty())
			return new HttpResponse(HttpStatus.OK.value(), "Product not found.");
		Product getProduct = getProducts.get();
		if(product.getProduct_name()!=null) 
			getProduct.setProduct_name(product.getProduct_name());
		if(product.getCategory()!=null) 
			getProduct.setCategory(product.getCategory());
		if(product.getStock()!=0) 
			getProduct.setStock(product.getStock());
		if(product.getPrice()!=0)
			getProduct.setPrice(product.getPrice());
		if(!validateProduct(getProduct))
			return new HttpResponse(HttpStatus.OK.value(), "Update failed, check stock and price.");
		Product updatedProduct = repo.save(getProduct);
		return new HttpResponse(HttpStatus.OK.value(), "Update successfull", updatedProduct);
	}

	@DeleteMapping(value = "/api/product/{id}", produces = "application/json")
	public HttpResponse delete(@PathVariable long id) {
		Optional<Product> products = repo.findById(id);
		if(products.isEmpty())
			return new HttpResponse(HttpStatus.OK.value(), "Product not found");
		Product product = products.get();
		repo.delete(product);
		return new HttpResponse(HttpStatus.OK.value(), "Product deleted", product);
	}
	
	@PostMapping(value = "/api/search")
	public HttpResponse search(
			@RequestParam(name = "product_name", required = false) String product_name,
			@RequestParam(name = "category", required = false) String category,
			@RequestParam(name = "stock", required = false) String stock,
			@RequestParam(name = "price", required = false) String price
			) {
		List<Product> products = new ArrayList<Product>();
		if(product_name==null && stock==null && price==null && category==null)
			return new HttpResponse(HttpStatus.OK.value(), "No params provided", products);
		products = repo.search(product_name, stock, stock, category);
		return new HttpResponse(HttpStatus.OK.value(), "Success", products);
	}

}
