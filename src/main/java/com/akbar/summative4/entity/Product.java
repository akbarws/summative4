package com.akbar.summative4.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "product")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(nullable = false)
	private String product_name;
	@Column(nullable = false)
	private String category;
	@Column(nullable = false)
	private int stock;
	@Column(nullable = false)
	private int price;
	@CreationTimestamp
	@Column(nullable = false)
	private Date created_at;
	
	public Product() {}
	
	public Product(String product_name, String category, int stock, int price) {
		super();
		this.product_name = product_name;
		this.category = category;
		this.stock = stock;
		this.price = price;
	}

	public Product(long product_id) {
		this.id = product_id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", product_name=" + product_name + ", category=" + category + ", stock=" + stock
				+ ", price=" + price + ", created_at=" + created_at + "]";
	}
	
}
