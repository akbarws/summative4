package com.akbar.summative4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Summative4Application {

	public static void main(String[] args) {
		SpringApplication.run(Summative4Application.class, args);
	}

}
